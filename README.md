# rshmhrj's dotfiles

I'm in the process of setting things up again and want to [store all my customizations for future use and reference](https://dotfiles.github.io/inspiration/) -- hence this repo.

FYI - I have been an [avid user / proponent](https://rshmhrj.io/refs/zshrc/) of [oh-my-zsh](https://ohmyz.sh/), but have been having extremely long start-up times.  I have stripped everything out and am trying to figure out the best way to incorporate parts of plugins / functions / aliases from OMZ.

I plan on also looking into other multi-workstation config syncing tools like [mackup](https://github.com/lra/mackup)

## Setup

Main project / programming folder location for me: `/Users/myuser/Programming`

```bash
mkdir $PROG/__mine
cd __mine
git clone https://gitlab.com/rshmhrj/dotfiles.git
```

Dotfile location: `/Users/myuser/Programming/__mine/dotfiles`

Copy the `secret.example.zsh` file as `secret.zsh` and enter any secrets / API Keys / tokens

Use dotfiles' .zshrc with a symlink: `ln -s ~/Programming/__mine/dotfiles/zsh/.zshrc ~/.zshrc`
Use dotfiles' .shellcheckrc with a symlink: `ln -s ~/Programming/__mine/dotfiles/.shellcheckrc ~/.shellcheckrc`
