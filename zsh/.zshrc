# \m/_ ... _\m/ #
# zmodload zsh/zprof

# start=$SECONDS
start=$(gdate +%s%N | cut -b1-13)

# load autocompletions
# autoload -Uz compinit
# compinit

# ENV & SECRETS
[[ -f ~/Programming/__mine/dotfiles/zsh/env.zsh ]] && source ~/Programming/__mine/dotfiles/zsh/env.zsh
# pathEnd=$SECONDS

# FUNCTIONS
[[ -f ~/Programming/__mine/dotfiles/zsh/function.zsh ]] && source ~/Programming/__mine/dotfiles/zsh/function.zsh
# funcDefEnd=$SECONDS

# USER CONFIG
[[ -f ~/Programming/__mine/dotfiles/zsh/alias.zsh ]] && source ~/Programming/__mine/dotfiles/zsh/alias.zsh

# HISTORY CONFIG
[[ -f ~/Programming/__mine/dotfiles/zsh/history.zsh ]] && source ~/Programming/__mine/dotfiles/zsh/history.zsh
# aliasEnd=$SECONDS

# GPG
export GPG_TTY=$(tty)
# gpgconf --launch gpg-agent
# gpgEnd=$SECONDS

# maildevRunning
# maildevEnd=$SECONDS

vtr &>/dev/null
# vaultEnd=$SECONDS

# eval "$(starship init zsh)"
# ssEnd=$SECONDS

# eval "$(atuin init zsh)"
eval "$(command jenv init -)"

# start cloudflare tunnel & stable_diffusion
cf_tunnel_install &>/dev/null
start_sd &>/dev/null

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
# __conda_setup="$('/Users/rmaharaj017/opt/anaconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
# if [ $? -eq 0 ]; then
#     eval "$__conda_setup"
# else
#     if [ -f "/Users/rmaharaj017/opt/anaconda3/etc/profile.d/conda.sh" ]; then
#         . "/Users/rmaharaj017/opt/anaconda3/etc/profile.d/conda.sh"
#     else
#         export PATH="/Users/rmaharaj017/opt/anaconda3/bin:$PATH"
#     fi
# fi
# unset __conda_setup
# <<< conda initialize <<<
# condaEnd=$SECONDS

# end=$SECONDS
end=$(gdate +%s%N | cut -b1-13)

# echo timing:
# echo "start -> path -> func -> alias -> gpg -> maildev -> vault -> ss -> conda -> end"
# echo "         $(( pathEnd - start ))s   -> $(( funcDefEnd - pathEnd ))s   -> $(( aliasEnd - funcDefEnd ))s    -> $(( gpgEnd - aliasEnd ))s  -> $(( maildevEnd - gpgEnd ))s      -> $(( vaultEnd - maildevEnd ))s    -> $(( ssEnd - vaultEnd ))s -> $(( condaEnd - ssEnd ))s    -> $(( end - condaEnd ))s"
# echo "total: $(( end - start ))s"
# echo $end - $start
echo "load in $((end - start))ms"

# zprof
