# ALIAS

## super user alias
alias _='sudo '

## ai
# alias ollama=docker exec -it ollama ollama 

# echo "atuin"
# alias asi='atuin search -i'

# echo "java"
alias java8='export JAVA_HOME=$JAVA_8_HOME'
alias java17='export JAVA_HOME=$JAVA_17_HOME'
alias java20='export JAVA_HOME=$JAVA_20_HOME'
alias mcip='mvn clean install package -U -DskipTests'
alias mcipt='mvn clean install package -U'

# echo "cmd"
[[ -f $DF/zsh/omz/dir.zsh ]] && source $DF/zsh/omz/dir.zsh
alias date='gdate'
alias reload='source ~/.zshrc'
alias cls=clear
alias psl='lsof -i -P | grep LISTEN'
alias psf="ps -e | grep -e $1"
alias catl="cat $1 | less"

# echo "gcloud"
alias gcssh="gcloud alpha cloud-shell ssh"

# echo "git"
[[ -f $DF/zsh/omz/git.zsh ]] && source $DF/zsh/omz/git.zsh
alias gcmm="gcm;gl;gfa"
alias gmm="git merge master"
alias gc-="git checkout -"
alias gcamnv="gcam --no-verify"
alias ggpnv="ggp --no-verify"

# echo "docker-compose"
[[ -f $DF/zsh/omz/docker-compose.zsh ]] && source $DF/zsh/omz/docker-compose.zsh

# echo "kubectl"
[[ -f $DF/zsh/omz/kubectl.zsh ]] && source $DF/zsh/omz/kubectl.zsh
alias kgmci="k get mci"
alias kgmcs="k get mcs"
alias kdmci="k describe mci"
alias kdmcs="k describe mcs"
alias kcrc="kubectl config rename-context"
alias kgpsi="kgp $NO && kgs $NO && kgi $NO"
alias kgpv="kubectl get pv"
alias kepv="kubectl edit pv"
alias kdpv="kubectl describe pv"
alias kdelpv="kubectl delete pv"
alias kstg="kubectl -n jx-staging"
export NO=--insecure-skip-tls-verify
export NOS="--insecure-skip-tls-verify --show-labels"
export NOH=--kube-insecure-skip-tls-verify

# echo "go"
alias grmg="go run main.go"

# echo "node"
alias node14='export PATH="/usr/local/opt/node@14/bin:$PATH"'
# alias node14='export NODE_HOME=$NODE_14_HOME'
# alias node16='export NODE_HOME=$NODE_16_HOME'
alias node21='export NODE_HOME=$NODE_LATEST_HOME'

# echo "vault"
alias vtnp='export VAULT_TOKEN=$VAULT_TOKEN_NON_PROD; export VAULT_ADDR=$VAULT_ADDR_NON_PROD'
alias vtp='export VAULT_TOKEN=$VAULT_TOKEN_PROD; export VAULT_ADDR=$VAULT_ADDR_PROD'
alias vteunp='export VAULT_TOKEN=$VAULT_TOKEN_EU_NPE; export VAULT_ADDR=$VAULT_ADDR_EU_NPE'
alias vtrnp='vtnp && screen -m -d $vault token renew$'
alias vtrp='vtp && screen -m -d $vault token renew$'
alias vtreunp='vteunp && screen -m -d $vault token renew$'
# alias vtr='vtrnp && vtrp && vteunp'
alias vtr='vteunp'

# echo "google sdk"
source "$(brew --prefix)/share/google-cloud-sdk/path.zsh.inc"
source "$(brew --prefix)/share/google-cloud-sdk/completion.zsh.inc"
alias gast='google-oauthlib-tool --client-secrets $PROG/__util/google-assistant-sdk/$GAST_SECRET --scope https://www.googleapis.com/auth/assistant-sdk-prototype --save --headless'
alias gass="pushtotalk.py --device-model-id 'alfred' --device-id 'mbp64'"

# echo "utils"
# alias code='codium'
alias fetchip=fetchIp
alias getip=fetchip
alias getIp=fetchip
alias myip=fetchip
alias myIp=fetchip
alias getpass='kubectl get secret --namespace default ytc-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo'
alias ducks='du -cksh -- * | sort -hr'

# echo "postgres"
alias pg='psql -U postgres -h localhost'

# echo "dgraph"
alias rdg="rdg0 &; rdga"