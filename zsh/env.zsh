# VARIABLES
export USER=rmaharaj017
export PROG=/Users/$USER/Programming
export DF=$PROG/__mine/dotfiles

# SECRETS
[[ -f $DF/zsh/secret.zsh ]] && source $DF/zsh/secret.zsh

# LANGUAGE
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

# PATH
# export ANDROID_HOME=/Users/$USER/Library/Android/sdk
export GOPATH=/Users/$USER/go
export GOROOT=/Users/$USER/go
export GROOVY_HOME=/usr/local/opt/groovy/libexec
# export LIQUIBASE_HOME=/usr/local/opt/liquibase/libexec
export JENV_HOME=$HOME/.jenv/bin
# export JAVA_8_HOME=/usr/local/opt/openjdk@8/bin
# export JAVA_11_HOME=$(/usr/libexec/java_home -v11)
# export JAVA_17_HOME=/usr/local/opt/openjdk/bin
# export JAVA_HOME=$JAVA_17_HOME
export CPPFLAGS='-I/usr/local/opt/openjdk/include'
export MAVEN_HOME='/Users/rmaharaj017/.m2'
export MLC_HOME=/Users/rmaharaj017/Programming/__mine/python-study/scripts
export MOJO_PATH='/Users/rmaharaj017/.modular/pkg/packages.modular.com_mojo/bin'
export MODULAR_HOME='/Users/rmaharaj017/.modular'
export NODE_EXTRA_CA_CERTS=$PROG/__certs/pwccerts/pwcBundle.pem
export NODE_HOME='/usr/local/opt/node/bin'
export NODE_14_HOME='/usr/local/opt/node@14/bin'
export NODE_LATEST_HOME='/usr/local/opt/node/bin'
export NODE_EXTRA_CA_CERTS=$PROG/__certs/PwC-Root-ca-public/pwc-root-13.pem
export NVM_HOME=/opt/homebrew/opt/nvm
export NVM_DIR="$HOME/.nvm"
export NB4='~/nb4'
export OLLAMA_ORIGINS='app://obsidian.md*'
export PIPX='/Users/rmaharaj017/.local/bin'
path=(
  $path
  /usr/local/bin
  /usr/local/homebrew/bin
  # $ANDROID_HOME/tools
  # $ANDROID_HOME/platform-tools
  $HOME/bin
  $GOPATH/bin
  $GOROOT/bin
  $GROOVY_HOME
  $JENV_HOME
  # $JAVA_HOME
  # $JAVA_8_HOME
  # $JAVA_11_HOME
  # $JAVA_17_HOME
  # $LIQUIBASE_HOME
  $MAVEN_HOME
  $MOJO_PATH
  $MODULAR_HOME
  $NODE_HOME
  $NB4
  $PIPX
  $MLC_HOME
  # $PROG/__util/google-assistant-sdk/assistant-sdk-python/google-assistant-sdk/googlesamples/assistant/grpc
)

path=($^path(N))
