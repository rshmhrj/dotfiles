# CUSTOM FUNCS

# cd into a directory and see its contents
function cs () {
    builtin cd "$@" && ls
}

## kubectl rename this context
function kcrtc() {
  if [ "$1" != "" ]
  then
    kcrc $(kccc) $1
  fi
}

function listening() {
  if [ $# -eq 0 ]; then
      lsof -iTCP -sTCP:LISTEN -n -P
  elif [ $# -eq 1 ]; then
      lsof -iTCP -sTCP:LISTEN -n -P | grep -i --color $1
  else
      echo "Usage: listening [pattern]"
  fi
}


## kill process running on given port
function kp() {
  if [ $# -eq 0 ]; then
    echo "Usage: kp [port]"
    echo "e.g.: kp 8098"
  else
    lsof -t -i:$1 | xargs kill -9
  fi
}

## get current external IP address via google nameserver lookup
function fetchIp() {
  echo My WAN/Public IP: $(dig TXT +short +tries=1 +time=1 retry=0 o-o.myaddr.l.google.com @ns1.google.com | sed 's/\"//g')
}

## maildev
### checks if maildev is currently running.  if it isn't, then start it.
function maildevRunning {
  [[ $(lsof -i tcp:1025 | wc -l) -eq 0 ]] && echo "maildev is not running; starting it." && docker run -p 1080:1080 -p 1025:1025 maildev/maildev &;
}

rmd() {
  docker run -p 1080:1080 -p 1025:1025 maildev/maildev &;
}

function brewRefresh {
  if [[ $(date +%u) -eq 0 ]]; then echo true; else echo false; fi
}

## docker
### start mongo docker
function rmdb {
  if [ "$1" != "" ]
  then
    docker run --rm -it \
      --name local-mongo-$1 \
      -p 27017:27017 \
      -v $PROG/__data/mongodb/$1:/etc/mongo \
      -d mongo
  else
    docker run --rm -it \
      --name local-mongo-local \
      -p 27017:27017 \
      -v $PROG/__data/mongodb/local:/etc/mongo \
      -d mongo
  fi
}

### start postgres docker
export PGDATA=$PROG/__data/postgres
function rpg {
  docker run -d --rm -it --name local-pg -e POSTGRES_USER=admin -e POSTGRES_PASSWORD=Letme!n123 -p 5433:5432 -v $PGDATA/activeBkp:/docker-entrypoint-initdb.d -v $PGDATA/pgdata:/var/lib/postgresql/data postgres
}


### pulsar
# function rp {
#   docker run --rm -it -d \
#     -p 6650:6650 \
#     -p 8090:8090 \
#     -v $PROG/__data:/pulsar/data \
#     apachepulsar/pulsar:2.9.3 \
#     bin/pulsar standalone
# }

### jaeger
function rj {
  docker run -d --name jaeger \
  -e COLLECTOR_ZIPKIN_HOST_PORT=:9411 \
  -p 5775:5775/udp \
  -p 6831:6831/udp \
  -p 6832:6832/udp \
  -p 5778:5778 \
  -p 16686:16686 \
  -p 14268:14268 \
  -p 14250:14250 \
  -p 9411:9411 \
  jaegertracing/all-in-one:1.22
}

### unleash
function ru= {
  docker run \
   -e UNLEASH_PROXY_SECRETS=unleash-proxy \
   -e UNLEASH_URL='$UNLEASH_URL' \
   -e UNLEASH_API_TOKEN=$UNLEASH_API_TOKEN \
   -p 3000:3000 \
   unleashorg/unleash-proxy
}

### dgraph
# function rdg0 {
#   docker run --rm -it \
#   -p 5080:5080 -p 6080:6080 -p 8080:8080 \
#   -p 9080:9080 -p 8000:8000 \
#   -v $PROG/__data/dgraph:/dgraph \
#   --name dgraph \
#   dgraph/dgraph:v21.03.0 dgraph zero
# }

# function rdga {
#   docker exec -it dgraph dgraph alpha \
#   --cache size-mb=4096 --zero localhost:5080 --security whitelist=0.0.0.0/0
# }

# jenv() {
#   eval "$(command jenv init -)"
#   jenv "$@"
# }

### update java versions in jenv
function update_jenv {
  for i in (ls /Library/Java/JavaVirtualMachines/) ; 
    do jenv add "/Library/Java/JavaVirtualMachines/$i/Contents/Home" ;
  done;
}

lazynvm() {
  unset -f nvm node npm
  # [ -s “$NVM_HOME/nvm.sh” ] && . “$NVM_HOME/nvm.sh”
  [ -s "$NVM_HOME/nvm.sh" ] && \. "$NVM_HOME/nvm.sh"  # This loads nvm
  [ -s "$NVM_HOME/etc/bash_completion.d/nvm" ] && \. "$NVM_HOME/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
}

nvm() {
  lazynvm
  nvm $@
}

node() {
  lazynvm
  node $@
}

npm() {
  lazynvm
  npm $@
}

cf_tunnel_install() {
  [[ $(ps -aef | grep /opt/homebrew/bin/cloudflared | wc -l) -eq 0 ]] && echo "cloudflared is not running; starting it." && cloudflared service install $CLOUDFLARE_TUNNEL_TOKEN
}

start_sd() {
  if [[ $(lsof -i tcp:7861 | wc -l) -eq 0 ]]; then
    echo "stable_diff is not running; starting it."
    builtin cd /Users/rmaharaj017/Programming/__mine/stablediff
    poetry env use 3.10.8
    poetry run bash stable-diffusion-webui/webui.sh --listen --api --skip-torch-cuda-test --disable-safe-unpickle --no-half --disable-nan-check --disable-model-loading-ram-optimization > output.log &;
    cd-
  fi
}