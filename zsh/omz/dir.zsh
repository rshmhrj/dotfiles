# Changing/making/removing directory
setopt auto_cd
setopt auto_pushd
setopt pushd_ignore_dups
setopt pushdminus

# override cd
function cd() {
    # cd ..
    if [ "$*" = "builtin cd .." ]; then ..
    # plain cd go home
    elif [ $# -eq 0 ]; then builtin cd ${HOME} && ls
    # dir
    elif [[ -d "$*" ]] then builtin cd "$*" && ls
    # file, so go to file's dir
    elif [[ -f "$*" ]] then
      local dir=$(dirname "$*")
      builtin cd "$dir" && ls
    # catch all
    else $($*)
    fi;
}

alias -g ..='builtin cd .. && ls'
alias -g ...='builtin cd ../.. && ls'
alias -g ....='builtin cd ../../.. && ls'
alias -g .....='builtin cd ../../../.. && ls'
alias -g ......='builtin cd ../../../../.. && ls'

alias -- -='builtin cd - && ls'
alias 1='builtin cd -1 && ls'
alias 2='builtin cd -2 && ls'
alias 3='builtin cd -3 && ls'
alias 4='builtin cd -4 && ls'
alias 5='builtin cd -5 && ls'
alias 6='builtin cd -6 && ls'
alias 7='builtin cd -7 && ls'
alias 8='builtin cd -8 && ls'
alias 9='builtin cd -9 && ls'

alias md='mkdir -p'
alias rd=rmdir

function d () {
  if [[ -n $1 ]]; then
    dirs "$@"
  else
    dirs -v | head -n 10
  fi
}
# compdef _dirs d

# List directory contents
alias l='ls -lah'
alias ll='ls -lh'
alias lll="ls -lah"
alias lsa='ls -lah'
alias la='ls -lAh'

# mkcd is equivalent to takedir
function mkcd takedir() {
  mkdir -p $@ && cd ${@:$#}
}

function takeurl() {
  local data thedir
  data="$(mktemp)"
  curl -L "$1" > "$data"
  tar xf "$data"
  thedir="$(tar tf "$data" | head -n 1)"
  rm "$data"
  cd "$thedir"
}

function takegit() {
  git clone "$1"
  cd "$(basename ${1%%.git})"
}

function take() {
  if [[ $1 =~ ^(https?|ftp).*\.(tar\.(gz|bz2|xz)|tgz)$ ]]; then
    takeurl "$1"
  elif [[ $1 =~ ^([A-Za-z0-9]\+@|https?|git|ssh|ftps?|rsync).*\.git/?$ ]]; then
    takegit "$1"
  else
    takedir "$@"
  fi
}
