# VARIABLES / SECRETS
export UNLEASH_URL=https://gitlab.com/api/v4/feature_flags/unleash/projectId?instance_id=instanceId
export UNLEASH_API_TOKEN=tokenValue
export VAULT_TOKEN_NON_PROD=tokenValue
export VAULT_ADDR_NON_PROD=https://vault.example.com
export RESCUETIME_API_KEY=tokenValue
export GAST_SECRET=secretValue.apps.googleusercontent.com.json